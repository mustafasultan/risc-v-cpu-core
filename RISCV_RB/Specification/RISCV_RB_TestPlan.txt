Input signals      ce   RWr  rd     WBDat    rs1    rs2
Input signal radix 1'b  1'b  5'b    32'h     5'b    5'b
Output signals                                              rs1D     rs2D
Output signal radix                                         32'h     32'h
TestNo  delay                                                                  Note
1       1          1    1    00100  deadbeef  00100  00100  deadbeef deadbeef  Write 0xdeadbeef to RB(4), Read RB(4) on rs1D and rs2D
2       1          1    0    00100  f00dcafe  00100  00100  f00dcafe ffffffff  Read RB(4) only on rs1D
3       1          0    1    00100  deadbeef  00100  00100  deadbeef deadbeef  Write 0xdeadbeef to RB(4) only, no read
4       1          1    1    00100  c001100c  00100  00101  c001100c ffffffff  Write 0xc001100c to RB(4), Read RB(5) on rs2D
5       1          1    1    11111  a5a5a5a5  00100  00101  a5a5a5a5 deadbeef  Write 0xa5a5a5a5 to RB(31), Read RB(5) on rs2D
6       1          1    1    00000  3c3c3c3c  00000  00000  3c3c3c3c 3c3c3c3c  Write 0x3c3c3c3c to RB(0), Read RB(0) on rs1D and rs2D
7       2          1    1    00000  5a5a5a5a  11111  00100  5a5a5a5a f00dcafe  Delayed Write 0x5a5a5a5a to RB(0), Read RB(4) on rs1D and rs2D
8       1          1    1    00111  ffffffff  11010  01100  ffffffff f00dcafe  Write 0xffffffff to RB(7), Read RB(12) on rs2D
9       1          0    0    00100  a5a5a5a5  00100  00100  00000000 00000000  No operation (ce=0, RWr=0)
10      1          1    1    01000  c001cafe  00100  00000  c001cafe c001100c  Write 0xc001cafe to RB(8), Read RB(0) on rs1D and rs2D



Complete test plan, generate HDL and simulate HDL model
Use hex values such as deadbeef, c001cafe, f00dcafe, c001100c, a5a5a5a5, 3c3c3c3c, 5a5a5a5a, ffffffff
Verify combinational output signals by simulation for <1, e.g, delayP in delay column. Always use delay = 1 - delayP in the next test plan row.
e.g, 
6       0.2          1    1    00100  deadbeef  00100  00100  deadbeef  deadbeef  Write RB(4),        Read RB(4) on rs1D  and rs2D
7       0.8          1    1    00100  deadbeef  00100  00100  deadbeef  deadbeef  Write RB(4),        Read RB(4) on rs1D  and rs2D

-- === Test Specification / Test Table Notes ===
Title: RV32I RISC-V processor Resister Bank (RB)
	   Test table (input signals and expected outputs)
Created by: Fearghal Morgan
Date: July 2023
Component type (include * to highlight)
Combinational()
Sequential (*) 

Notes on the use of the Test Table with HDLGen-ChatGPT (https://github.com/fearghal1/HDLGen-ChatGPT)
1. Copy/paste the test table (excluding the header text) into the HDLGen-ChatGPT 'Test Plan' UI box.
2. Test table structure
     Use only single character spacing in this file. Do not use TAB spacing in this file.
     e.g, if using Notepad++ (https://notepad-plus-plus.org/downloads/), toggle 'make all characters visible' option to view space/TAB characters.
     Row 1 includes input signal name headings
     Row 2 includes output signal name headings
     Row 3 includes signal radix
     Row 4 includes the following column headings (Column 1: TestNo, Column 2: delay, Other: Note)
     Rows 5 onwards includes test data
3. Signal radix format: 1'b is 1-bit binary (e.g, 0, 1), 16'b is 16-bit binary (e.g, 1011111011101111), 16'h is 16-bit hexadecimal (e.g, beef).
4. Notes column text is optional, providing useful information on the individual test.
5. Sequential logic components
     clk signal: testbench HDL includes a clock (signal clk) strobe stimulus process, with default 20ns (50MHz) 'period'.
     rst (reset) signal: initially asserted, deasserting 0.2 * period after the first active clk edge.
6. Table delay values
     Combinational logic components: number of period (20ns) delays to be included, following the application of the test input signals 
     Sequential logic components:
       Number of clock periods to be applied, following  the application of the test input signals. Value can be integer or real.
       Examples:
         1:   delay 1 clk period,   ending 0.2 x period after the active clk edge
         3:   delay 3 clk period,   ending 0.2 x period after the active clk edge
         0.2: delay 0.2 clk period, ending 0.4 x period after the active clk edge
              This enables simulation ending 0.6*period prior to the next active clk edge, enabling checking of unregistered output signals
         0.8: delay 0.8 clk period, ending at the default point, i.e, 0.2 x period after the active clk edge