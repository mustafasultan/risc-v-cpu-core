-- Title Section Start
-- VHDL testbench RISCV_RB_TB
-- Generated by HDLGen, Github https://github.com/fearghal1/HDLGen-ChatGPT, on 16-October-2023 at 15:50

-- Component Name: RISCV_RB
-- Title: 32 x 32-bit Register Bank, with chip enable. Single synchronous write port, dual combinational read ports

-- Author(s): Mustafa Sultan
-- Organisation: University of Galway
-- Email: m.sultan2@universityofgalway.ie
-- Date: 16/10/2023

-- Description: refer to component hdl model for function description and signal dictionary
-- Title Section End

-- Library declarations
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Testbench entity declaration. No inputs or outputs
entity RISCV_RB_TB is
end entity RISCV_RB_TB;

architecture behave of RISCV_RB_TB is

  -- Unit under test (UUT) component declaration. Identical to component entity, with 'entity' replaced with 'component'
  component RISCV_RB is
    Port(
      RWr   : in std_logic;
      rd    : in std_logic_vector(4 downto 0);
      rs1   : in std_logic_vector(4 downto 0);
      rs2   : in std_logic_vector(4 downto 0);
      rs1D  : out std_logic_vector(31 downto 0);
      rs2D  : out std_logic_vector(31 downto 0);
      WBDat : in std_logic_vector(31 downto 0);
      ce    : in std_logic;
      clk   : in std_logic;
      rst   : in std_logic
    );
  end component;

  -- Testbench signal declarations
  signal testNo: integer; -- Aids locating test in the simulation waveform
  signal endOfSim : boolean := false; -- Assert at the end of simulation to highlight simulation done. Stops clk signal generation.

  -- Typically use the same signal names as in the VHDL entity, with the keyword "signal" added, and without "in/out" mode keywords

  signal clk: std_logic := '1';
  signal rst: std_logic;

  signal RWr : std_logic;
  signal rd : std_logic_vector(4 downto 0);
  signal rs1 : std_logic_vector(4 downto 0);
  signal rs2 : std_logic_vector(4 downto 0);
  signal rs1D : std_logic_vector(31 downto 0);
  signal rs2D : std_logic_vector(31 downto 0);
  signal WBDat : std_logic_vector(31 downto 0);
  signal ce : std_logic;

  constant period: time := 20 ns; -- Default simulation time. Use as simulation delay constant, or clk period if sequential model (50MHz clk here)

begin

  -- Generate clk signal, when endOfSim = FALSE / 0
  clkStim: clk <= not clk after period/2 when endOfSim = false else '0';

  -- Instantiate unit under test (UUT)
  UUT: RISCV_RB -- Map component internal signals to testbench signals
  port map (
    RWr   => RWr,
    rd    => rd,
    rs1   => rs1,
    rs2   => rs2,
    rs1D  => rs1D,
    rs2D  => rs2D,
    WBDat => WBDat,
    ce    => ce,
    clk   => clk,
    rst   => rst
  );

  -- Signal stimulus process
  stim_p: process -- Process sensitivity list is empty, so the process automatically executes at the start of the simulation. Suspend the process at the "wait;" statement
  begin
    report "%N Simulation start, time = " & time'image(now);

    -- Apply default INPUT signal values. Do not assign output signals (generated by the UUT) in this stim_p process
    -- Each stimulus signal change occurs 0.2 * period after the active low-to-high clk edge
    -- if the signal type is
    -- std_logic, use '0'
    -- std_logic_vector use (others => '0')
    -- integer use 0
    RWr <= '0';
    rd <= (others => '0');
    rs1 <= (others => '0');
    rs2 <= (others => '0');
    WBDat <= (others => '0');
    ce <= '0';
    report "Assert and toggle rst";
    testNo <= 0;
    rst <= '1';
    wait for period * 1.2; -- Assert rst for 1.2 * period, deasserting rst 0.2 * period after the active clk edge
    rst <= '0';
    wait for period; -- Wait 1 clock period

    -- Test 1: Write RB(4), Read RB(4) on rs1D and rs2D
    testNo <= 1;
    -- Input signal assignments
    ce <= '1';
    RWr <= '1';
    rd <= "00100";
    WBDat <= x"deadbeef"; -- Hex value marked as requested
    rs1 <= "00100";
    rs2 <= "00100";
    -- Delay statement
    wait for 1 * period;
    -- Output signal comparisons
    assert rs1D = x"deadbeef" report "Test 1 failed for rs1D" severity warning;
    assert rs2D = x"deadbeef" report "Test 1 failed for rs2D" severity warning;

    -- Test 2: Read RB(4) only on rs1D
    testNo <= 2;
    -- Input signal assignments
    ce <= '1';
    RWr <= '0';
    rd <= "00100";
    WBDat <= x"deadbeef"; -- Hex value marked as requested
    rs1 <= "00100";
    rs2 <= "00100";
    -- Delay statement
    wait for 1 * period;
    -- Output signal comparisons
    assert rs1D = x"deadbeef" report "Test 2 failed for rs1D" severity warning;
    assert rs2D = x"ffffffff" report "Test 2 failed for rs2D" severity warning;

    -- Test 3: Write RB(4) only, no read
    testNo <= 3;
    -- Input signal assignments
    ce <= '1';
    RWr <= '1';
    rd <= "00100";
    WBDat <= x"deadbeef"; -- Hex value marked as requested
    rs1 <= "00100";
    rs2 <= "00100";
    -- Delay statement
    wait for 1 * period;
    -- Output signal comparisons
    assert rs1D = x"deadbeef" report "Test 3 failed for rs1D" severity warning;
    assert rs2D = x"deadbeef" report "Test 3 failed for rs2D" severity warning;

    -- Test 4: Write RB(4), Read RB(5) on rs2D
    testNo <= 4;
    -- Input signal assignments
    ce <= '1';
    RWr <= '1';
    rd <= "00100";
    WBDat <= x"deadbeef"; -- Hex value marked as requested
    rs1 <= "00100";
    rs2 <= "00101";
    -- Delay statement
    wait for 1 * period;
    -- Output signal comparisons
    assert rs1D = x"deadbeef" report "Test 4 failed for rs1D" severity warning;
    assert rs2D = x"deadbeef" report "Test 4 failed for rs2D" severity warning;

    -- Test 5: Write RB(31), Read RB(5) on rs2D
    testNo <= 5;
    -- Input signal assignments
    ce <= '1';
    RWr <= '1';
    rd <= "11111";
    WBDat <= x"c001cafe"; -- Hex value marked as requested
    rs1 <= "00100";
    rs2 <= "00101";
    -- Delay statement
    wait for 1 * period;
    -- Output signal comparisons
    assert rs1D = x"deadbeef" report "Test 5 failed for rs1D" severity warning;
    assert rs2D = x"deadbeef" report "Test 5 failed for rs2D" severity warning;

    -- Test 6: Write RB(0), Read RB(0) on rs1D and rs2D
    testNo <= 6;
    -- Input signal assignments
    ce <= '1';
    RWr <= '1';
    rd <= "00000";
    WBDat <= x"3c3c3c3c"; -- Hex value marked as requested
    rs1 <= "00000";
    rs2 <= "00000";
    -- Delay statement
    wait for 1 * period;
    -- Output signal comparisons
    assert rs1D = x"3c3c3c3c" report "Test 6 failed for rs1D" severity warning;
    assert rs2D = x"3c3c3c3c" report "Test 6 failed for rs2D" severity warning;

    -- Test 7: Delayed Write RB(0), Read RB(4) on rs1D and rs2D
    testNo <= 7;
    -- Input signal assignments
    ce <= '1';
    RWr <= '1';
    rd <= "00000";
    WBDat <= x"f00dcafe"; -- Hex value marked as requested
    rs1 <= "11111";
    rs2 <= "00100";
    -- Delay statement
    wait for 2 * period;
    -- Output signal comparisons
    assert rs1D = x"f00dcafe" report "Test 7 failed for rs1D" severity warning;
    assert rs2D = x"f00dcafe" report "Test 7 failed for rs2D" severity warning;

    -- Test 8: Write RB(7), Read RB(12) on rs2D
    testNo <= 8;
    -- Input signal assignments
    ce <= '1';
    RWr <= '1';
    rd <= "00111";
    WBDat <= x"a5a5a5a5"; -- Hex value marked as requested
    rs1 <= "11010";
    rs2 <= "01100";
    -- Delay statement
    wait for 1 * period;
    -- Output signal comparisons
    assert rs1D = x"a5a5a5a5" report "Test 8 failed for rs1D" severity warning;
    assert rs2D = x"ffffffff" report "Test 8 failed for rs2D" severity warning;

    -- Test 9: No operation (ce=0, RWr=0)
    testNo <= 9;
    -- Input signal assignments
    ce <= '0';
    RWr <= '0';
    rd <= "00100";
    WBDat <= x"c001100c"; -- Hex value marked as requested
    rs1 <= "00100";
    rs2 <= "00100";
    -- Delay statement
    wait for 1 * period;
    -- Output signal comparisons
    assert rs1D = x"00000000" report "Test 9 failed for rs1D" severity warning;
    assert rs2D = x"00000000" report "Test 9 failed for rs2D" severity warning;

    -- Test 10: Write RB(8), Read RB(0) on rs1D and rs2D
    testNo <= 10;
    -- Input signal assignments
    ce <= '1';
    RWr <= '1';
    rd <= "01000";
    WBDat <= x"5a5a5a5a"; -- Hex value marked as requested
    rs1 <= "00100";
    rs2 <= "00000";
    -- Delay statement
    wait for 1 * period;
    -- Output signal comparisons
    assert rs1D = x"5a5a5a5a" report "Test 10 failed for rs1D" severity warning;
    assert rs2D = x"5a5a5a5a" report "Test 10 failed for rs2D" severity warning;

    -- END Add testbench stimulus here
    -- Print picosecond (ps) = 1000*ns (nanosecond) time to simulation transcript
    -- Use to find the time when the simulation ends (endOfSim is TRUE)
    -- Re-run the simulation for this time
    -- Select the timing diagram and use View>Zoom Fit
    report "%N Simulation end, time = " & time'image(now);
    endOfSim <= TRUE; -- Assert flag to stop clk signal generation

    wait; -- Wait forever
  end process;

end behave;
