

The table is included at the end of the prompt.


The format of the table is as follows
Row 1 includes input signal name headings
Row 2 includes output signal name headings
Row 3 includes signal radix
Row 4 includes the following column headings
Column 1: TestNo
Column 2: delay
Other:    Note
Rows 5 onwards includes test data

Create and output a single VHDL process labelled stim_p, in a formatted code box, including the following, for each TestNo row:
Line space, as test code separator
Integer signal TestNo, using TestNo <= assignment.
Note column field, as a VHDL comment.
Assignment of each input signal value with the value included in the corresponding table column, not omitting any input signal name and never assigning an output signal.
Delay statement, format 'wait for (delay * period);', where delay is an integer or real value, never 0, placing the delay statement after all of the signal stimulus for the TestNo have been assigned, and always including brackets around the delay statement.
Comparison of every output signal (not input signals) with the binary or hexadecimal value included in its corresponding output signal column, outputting a separate warning (not failure) message for each individual signal test (including TestNo) if the values do not match, and using a separate assert statement, and unique message for each one. Do not include any input or internal signal names in the comparison statement. Ensure correct bit width in comparisons.

For signal which have a digit boundary width, use hexadecimal format in constant assignments, with 'x' prefix before double quotation marks.
For all input signals, ensure use of the correct values and the correct signal widths.
Do not include any VHDL 'for' loops in the output.
Do not include 'for testNo in ..'  or 'case TestNo ..' statements.
Do not include any VHDL variables.
Do not assign any output signals.
Do not include any other delay statements.
Do not request that I '-- Continue with the remaining test cases...'. Please output code for all of the tests.
Do not request that I '-- Repeat similar sections for the remaining tests...'. Please output code for all of the tests.
Do not request that I '-- Comparison of output signals with expected values...'. Please output code for all of the tests.
Do not request that I '-- Continue similar sections for the remaining tests...'. Please output code for all of the tests.

signal testNo: integer;
signal period: time := 20 ns;
signal RWr : std_logic;
signal rd : std_logic_vector(4 downto 0);
signal rs1 : std_logic_vector(4 downto 0);
signal rs2 : std_logic_vector(4 downto 0);
signal rs1D : std_logic_vector(31 downto 0);
signal rs2D : std_logic_vector(31 downto 0);
signal WBDat : std_logic_vector(31 downto 0);
signal ce : std_logic;

Input signals      ce   RWr  rd     WBDat    rs1    rs2
Input signal radix 1'b  1'b  5'b    32'h     5'b    5'b
Output signals                                              rs1D      rs2D
Output signal radix                                         32'h      32'h
TestNo  delay                                                                  Note
1       1          1    1    00100  deadbeef  00100  00100  deadbeef  deadbeef  Write RB(4), Read RB(4) on rs1D 
and rs2D
2       1          1    0    00100  deadbeef  00100  00100  deadbeef  ffffffff  Read RB(4) only on rs1D
3       1          0    1    00100  deadbeef  00100  00100  deadbeef  deadbeef  Write RB(4) only, no read
4       1          1    1    00100  deadbeef  00100  00101  deadbeef  deadbeef  Write RB(4), Read RB(5) on rs2D
5       1          1    1    11111  c001cafe  00100  00101  deadbeef  deadbeef  Write RB(31), Read RB(5) on rs2D
6       1          1    1    00000  3c3c3c3c  00000  00000  3c3c3c3c  3c3c3c3c  Write RB(0), Read RB(0) on rs1D and 
rs2D
7       2          1    1    00000  f00dcafe  11111  00100  f00dcafe  f00dcafe  Delayed Write RB(0), Read RB(4) on 
rs1D and rs2D
8       1          1    1    00111  a5a5a5a5  11010  01100  a5a5a5a5  ffffffff  Write RB(7), Read RB(12) on rs2D
9       1          0    0    00100  c001100c  00100  00100  00000000  00000000  No operation (ce=0, RWr=0)
10      1          1    1    01000  5a5a5a5a  00100  00000  5a5a5a5a  5a5a5a5a  Write RB(8), Read RB(0) on rs1D 
and rs2D