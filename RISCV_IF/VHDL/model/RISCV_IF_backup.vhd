-- library declarations
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.MainPackage.all;

-- entity declaration
entity RISCV_IF is 
Port(
    clk : in std_logic;
    rst : in std_logic;
    ce : in std_logic;
    instruction : out std_logic_vector(31 downto 0);
    PC : out std_logic_vector(31 downto 0);
    PCPlus4 : out std_logic_vector(31 downto 0);
    selNxtPC : in std_logic;
    brAdd : in std_logic_vector(31 downto 0)
);
end entity RISCV_IF;

architecture RTL of RISCV_IF is
    -- Internal signal declarations
    signal intPC : std_logic_vector(31 downto 0);

begin

    -- PC_c: PC <= intPC;
    PC <= intPC;  -- Default assignment

    RISCV_PCCU_i: RISCV_PCCU
    port map(
        clk => clk,
        rst => rst,
        load => selNxtPC,
        loadDat => brAdd,
        ce => ce,
        count => intPC,
        countPlus4 => PCPlus4
    );

    RISCV_IM_i: RISCV_IM
    port map(
        PC => intPC,
        instruction => instruction
    );
end RTL;
