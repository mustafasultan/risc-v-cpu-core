library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.MainPackage.all;

-- entity declaration
entity RISCV_PCCU is
  Port(
    clk        : in std_logic;
    rst        : in std_logic;
    load       : in std_logic;
    loadDat    : in std_logic_vector(31 downto 0);
    ce         : in std_logic;
    count      : out std_logic_vector(31 downto 0);
    countPlus4 : out std_logic_vector(31 downto 0)
  );
end entity RISCV_PCCU;

architecture RTL of RISCV_PCCU is
  -- Internal signal declarations
  signal CS : std_logic_vector(31 downto 0) := (others => '0');
  signal NS : std_logic_vector(31 downto 0);

begin
  NSAndOPDecode_p: process(load, loadDat, ce, CS)
  begin
    NS <= std_logic_vector(unsigned(CS) + 4); -- Default assignment
    countPlus4 <= std_logic_vector(unsigned(CS) + 4); -- Default assignment
        
    if load = '1' then
        NS <= std_logic_vector(unsigned(loadDat)); 
        countPlus4 <= std_logic_vector(unsigned(loadDat) + 4);
    end if;
  end process;

  count_c: count <= CS;
    
  stateReg_p: process(clk, rst)
  begin
    if rst = '1' then
      CS <= (others => '0');
    elsif rising_edge(clk) then
      if ce = '1' then -- enable register
        CS <= NS;
      end if;
    end if;
  end process;

end RTL;