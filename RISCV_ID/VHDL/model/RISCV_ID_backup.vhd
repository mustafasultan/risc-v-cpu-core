-- library declarations
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.MainPackage.all;

-- entity declaration
entity RISCV_ID is 
Port(
	instruction : in std_logic_vector(31 downto 0);
	WBDat : in std_logic_vector(31 downto 0);
	rs1D : out std_logic_vector(31 downto 0);
	rs2D : out std_logic_vector(31 downto 0);
	selALUOp : out std_logic_vector(3 downto 0);
	selDToM : out std_logic_vector(1 downto 0);
	selNxtPC : out std_logic;
	selMToWB : out std_logic_vector(2 downto 0);
	selWBDat : out std_logic_vector(1 downto 0);
	MWr : out std_logic;
	MRd : out std_logic;
	selA : out std_logic;
	selB : out std_logic;
	selBrBaseAdd : out std_logic;
	ce : in std_logic;
	clk : in std_logic;
	rst : in std_logic;
	extImm : out std_logic_vector(31 downto 0);
	branch : in std_logic
);
end entity RISCV_ID;

architecture RTL of RISCV_ID is
	-- Internal signal declarations
	signal RWr : std_logic;
	signal rd : std_logic_vector(4 downto 0);
	signal rs1 : std_logic_vector(4 downto 0);
	signal rs2 : std_logic_vector(4 downto 0);

begin

	RB_i: RISCV_RB
	port map(
		clk => clk,
		rst => rst,
		RWr => RWr,
		rd => rd,
		rs1 => rs1,
		rs2 => rs2,
		rs1D => rs1D,
		rs2D => rs2D,
		WBDat => WBDat,
		ce => ce
	);

	DEC_i: RISCV_DEC
	port map(
		instruction => instruction,
		RWr => RWr,
		rd => rd,
		rs1 => rs1,
		rs2 => rs2,
		extImm => extImm,
		selA => selA,
		selB => selB,
		selALUOp => selALUOp,
		selDToM => selDToM,
		selBrBaseAdd => selBrBaseAdd,
		selMToWB => selMToWB,
		MWr => MWr,
		MRd => MRd,
		selWBDat => selWBDat,
		selNxtPC => selNxtPC,
		branch => branch
	);

end RTL;
