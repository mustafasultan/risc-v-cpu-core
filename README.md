# RISC-V CPU Core



## Getting started

A 32-bit RISC-V core written in VHDL.

## Features

- [ ] 32-bit RISC-V ISA CPU core.
- [ ] Supports RISC-V integer (I) instructions.
- [ ] Synthesizable VHDL and FPGA friendly.

