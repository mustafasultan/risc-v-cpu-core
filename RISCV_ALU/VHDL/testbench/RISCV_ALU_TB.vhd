-- Title Section Start
-- VHDL testbench RISCV_ALU_TB
-- Generated by HDLGen, Github https://github.com/fearghal1/HDLGen-ChatGPT, on 16-October-2023 at 16:44

-- Component Name : RISCV_ALU
-- Title          : RV32I RISC-V Arithmetic Logic Unit (ALU)

-- Author(s)      : Fearghal Morgan
-- Organisation   : University of Galway
-- Email          : fearghal.morgan@universityofgalway.ie
-- Date           : 16/10/2023

-- Description    : refer to component hdl model for function description and signal dictionary
-- Title Section End
-- library declarations
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Testbench entity declaration. No inputs or outputs
entity RISCV_ALU_TB is end entity RISCV_ALU_TB;

architecture behave of RISCV_ALU_TB is

-- unit under test (UUT) component declaration. Identical to component entity, with 'entity' replaced with 'component'
component RISCV_ALU is 
Port(
	selALUOp : in std_logic_vector(3 downto 0);
	A : in std_logic_vector(31 downto 0);
	B : in std_logic_vector(31 downto 0);
	ALUOut : out std_logic_vector(31 downto 0);
	branch : out std_logic 
	);
end component;

-- testbench signal declarations
signal testNo: integer; -- aids locating test in simulation waveform
signal endOfSim : boolean := false; -- assert at end of simulation to highlight simuation done. Stops clk signal generation.

-- Typically use the same signal names as in the VHDL entity, with keyword signal added, and without in/out mode keyword

signal selALUOp : std_logic_vector(3 downto 0);
signal A : std_logic_vector(31 downto 0);
signal B : std_logic_vector(31 downto 0);
signal ALUOut : std_logic_vector(31 downto 0);
signal branch : std_logic;

constant period: time := 20 ns; -- Default simulation time. Use as simulation delay constant, or clk period if sequential model ((50MHz clk here)
 
begin

-- instantiate unit under test (UUT)
UUT: RISCV_ALU-- map component internal sigs => testbench signals
port map
	(
	selALUOp => selALUOp, 
	A => A, 
	B => B, 
	ALUOut => ALUOut, 
	branch => branch
	);

-- Signal stimulus process
stim_p: process -- process sensitivity list is empty, so process automatically executes at start of simulation. Suspend process at the wait; statement
begin
	report "%N Simulation start, time = "& time'image(now);

	-- Apply default INPUT signal values. Do not assign output signals (generated by the UUT) in this stim_p process
	-- Each stimulus signal change occurs 0.2*period after the active low-to-high clk edge
	-- if signal type is
	-- std_logic, use '0'
	-- std_logic_vector use (others => '0')
	-- integer use 0
	selALUOp <= (others => '0');
	A <= (others => '0');
	B <= (others => '0');

    wait for 1 * period;
    TestNo <= 1; -- Assign TestNo
    selALUOp <= "0000"; -- Assign input signal selALUOp
    A <= x"5a5a5a5a"; -- Assign input signal A
    B <= x"15a5a5a6"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"70000000") report "Test 1 failed" severity warning;
    assert(branch = '0') report "Test 1 failed" severity warning;

    wait for 1 * period;
    TestNo <= 2; -- Assign TestNo
    selALUOp <= "0000"; -- Assign input signal selALUOp
    A <= x"15a5a5a6"; -- Assign input signal A
    B <= x"5a5a5a5a"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"70000000") report "Test 2 failed" severity warning;
    assert(branch = '0') report "Test 2 failed" severity warning;
    
    wait for 1 * period;
    TestNo <= 3; -- Assign TestNo
    selALUOp <= "0000"; -- Assign input signal selALUOp
    A <= x"ffffffff"; -- Assign input signal A
    B <= x"40000000"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"3fffffff") report "Test 3 failed" severity warning;
    assert(branch = '0') report "Test 3 failed" severity warning;

    wait for 1 * period;
    TestNo <= 4; -- Assign TestNo
    selALUOp <= "0001"; -- Assign input signal selALUOp
    A <= x"ffffffff"; -- Assign input signal A
    B <= x"fffffffe"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"00000001") report "Test 4 failed" severity warning;
    assert(branch = '0') report "Test 4 failed" severity warning;
    
    wait for 1 * period;
    TestNo <= 5; -- Assign TestNo
    selALUOp <= "0010"; -- Assign input signal selALUOp
    A <= x"f0c3a596"; -- Assign input signal A
    B <= x"1f7e8ab4"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"10428094") report "Test 5 failed" severity warning;
    assert(branch = '0') report "Test 5 failed" severity warning;

    wait for 1 * period;
    TestNo <= 6; -- Assign TestNo
    selALUOp <= "0011"; -- Assign input signal selALUOp
    A <= x"f0c3a596"; -- Assign input signal A
    B <= x"1f7e8ab4"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"ffffafb6") report "Test 6 failed" severity warning;
    assert(branch = '0') report "Test 6 failed" severity warning;

    wait for 1 * period;
    TestNo <= 7; -- Assign TestNo
    selALUOp <= "0100"; -- Assign input signal selALUOp
    A <= x"f0c3a596"; -- Assign input signal A
    B <= x"1f7e8ab4"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"efbd2f22") report "Test 7 failed" severity warning;
    assert(branch = '0') report "Test 7 failed" severity warning;

    wait for 1 * period;
    TestNo <= 8; -- Assign TestNo
    selALUOp <= "0101"; -- Assign input signal selALUOp
    A <= x"f0c3a596"; -- Assign input signal A
    B <= x"00000008"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"c3a59600") report "Test 8 failed" severity warning;
    assert(branch = '0') report "Test 8 failed" severity warning;

    wait for 1 * period;
    TestNo <= 9; -- Assign TestNo
    selALUOp <= "0110"; -- Assign input signal selALUOp
    A <= x"f0c3a596"; -- Assign input signal A
    B <= x"00000008"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"00f0c3a5") report "Test 9 failed" severity warning;
    assert(branch = '0') report "Test 9 failed" severity warning;

    wait for 1 * period;
    TestNo <= 10; -- Assign TestNo
    selALUOp <= "0111"; -- Assign input signal selALUOp
    A <= x"f0c3a596"; -- Assign input signal A
    B <= x"00000008"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"fff0c3a5") report "Test 10 failed" severity warning;
    assert(branch = '0') report "Test 10 failed" severity warning;

    wait for 1 * period;
    TestNo <= 11; -- Assign TestNo
    selALUOp <= "1000"; -- Assign input signal selALUOp
    A <= x"f0c3a596"; -- Assign input signal A
    B <= x"1f7e8ab4"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"00000001") report "Test 11 failed" severity warning;
    assert(branch = '0') report "Test 11 failed" severity warning;

    wait for 1 * period;
    TestNo <= 12; -- Assign TestNo
    selALUOp <= "1001"; -- Assign input signal selALUOp
    A <= x"f0c3a596"; -- Assign input signal A
    B <= x"1f7e8ab4"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"00000000") report "Test 12 failed" severity warning;
    assert(branch = '0') report "Test 12 failed" severity warning;

    wait for 1 * period;
    TestNo <= 13; -- Assign TestNo
    selALUOp <= "1010"; -- Assign input signal selALUOp
    A <= x"f0c3a596"; -- Assign input signal A
    B <= x"1f7e8ab4"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"00000000") report "Test 13 failed" severity warning;
    assert(branch = '0') report "Test 13 failed" severity warning;

    wait for 1 * period;
    TestNo <= 14; -- Assign TestNo
    selALUOp <= "1010"; -- Assign input signal selALUOp
    A <= x"f0c3a596"; -- Assign input signal A
    B <= x"f0c3a596"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"00000000") report "Test 14 failed" severity warning;
    assert(branch = '1') report "Test 14 failed" severity warning;

    wait for 1 * period;
    TestNo <= 15; -- Assign TestNo
    selALUOp <= "1011"; -- Assign input signal selALUOp
    A <= x"f0c3a596"; -- Assign input signal A
    B <= x"1f7e8ab4"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"00000000") report "Test 15 failed" severity warning;
    assert(branch = '1') report "Test 15 failed" severity warning;

    wait for 1 * period;
    TestNo <= 16; -- Assign TestNo
    selALUOp <= "1011"; -- Assign input signal selALUOp
    A <= x"f0c3a596"; -- Assign input signal A
    B <= x"f0c3a596"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"00000000") report "Test 16 failed" severity warning;
    assert(branch = '0') report "Test 16 failed" severity warning;

    wait for 1 * period;
    TestNo <= 17; -- Assign TestNo
    selALUOp <= "1100"; -- Assign input signal selALUOp
    A <= x"f0c3a596"; -- Assign input signal A
    B <= x"1f7e8ab4"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"00000000") report "Test 17 failed" severity warning;
    assert(branch = '1') report "Test 17 failed" severity warning;

    wait for 1 * period;
    TestNo <= 18; -- Assign TestNo
    selALUOp <= "1100"; -- Assign input signal selALUOp
    A <= x"f0c3a596"; -- Assign input signal A
    B <= x"f0c3a596"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"00000000") report "Test 18 failed" severity warning;
    assert(branch = '0') report "Test 18 failed" severity warning;

    wait for 1 * period;
    TestNo <= 19; -- Assign TestNo
    selALUOp <= "1101"; -- Assign input signal selALUOp
    A <= x"f0c3a596"; -- Assign input signal A
    B <= x"1f7e8ab4"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"00000000") report "Test 19 failed" severity warning;
    assert(branch = '0') report "Test 19 failed" severity warning;

    wait for 1 * period;
    TestNo <= 20; -- Assign TestNo
    selALUOp <= "1101"; -- Assign input signal selALUOp
    A <= x"f0c3a596"; -- Assign input signal A
    B <= x"f0c3a596"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"00000000") report "Test 20 failed" severity warning;
    assert(branch = '1') report "Test 20 failed" severity warning;

    wait for 1 * period;
    TestNo <= 21; -- Assign TestNo
    selALUOp <= "1110"; -- Assign input signal selALUOp
    A <= x"f0c3a596"; -- Assign input signal A
    B <= x"1f7e8ab4"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"00000000") report "Test 21 failed" severity warning;
    assert(branch = '0') report "Test 21 failed" severity warning;

    wait for 1 * period;
    TestNo <= 22; -- Assign TestNo
    selALUOp <= "1110"; -- Assign input signal selALUOp
    A <= x"f0c3a593"; -- Assign input signal A
    B <= x"f0c3a596"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"00000000") report "Test 22 failed" severity warning;
    assert(branch = '1') report "Test 22 failed" severity warning;

    wait for 1 * period;
    TestNo <= 23; -- Assign TestNo
    selALUOp <= "1111"; -- Assign input signal selALUOp
    A <= x"f0c3a596"; -- Assign input signal A
    B <= x"1f7e8ab4"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"00000000") report "Test 23 failed" severity warning;
    assert(branch = '1') report "Test 23 failed" severity warning;

    wait for 1 * period;
    TestNo <= 24; -- Assign TestNo
    selALUOp <= "1111"; -- Assign input signal selALUOp
    A <= x"f0c3a596"; -- Assign input signal A
    B <= x"f0c3a596"; -- Assign input signal B
    
    wait for 1 * period;
    
    assert(ALUOut = x"00000000") report "Test 24 failed" severity warning;
    assert(branch = '1') report "Test 24 failed" severity warning;

	-- END Add testbench stimulus here
	-- Print picosecond (ps) = 1000*ns (nanosecond) time to simulation transcript
	-- Use to find time when simulation ends (endOfSim is TRUE)
	-- Re-run the simulation for this time
	-- Select timing diagram and use View>Zoom Fit
	report "%N Simulation end, time = "& time'image(now);
	endOfSim <= TRUE; -- assert flag to stop clk signal generation

	wait; -- wait forever
end process; 
end behave;