
        # AMD-Xilinx Vivado project start and tcl script: Create project, xc7z020clg400-1 technology, %lang model 

        # To execute, 

        # open cmd window 

        # cd to project folder 

        # start Vivado (with tcl file parameter) 

        # e.g, for project name RISCV 

        # cmd 

        # cd {C:/HDLGen-ChatGPT/User_Projects/RISCV_EE451/RISCV} 

        # $vivado_bat_path -source C:/HDLGen-ChatGPT/User_Projects/RISCV_EE451/RISCV/VHDL/AMDPrj/RISCV.tcl 


        # Vivado tcl file RISCV.tcl, created in AMDprj folder 

        cd {C:/HDLGen-ChatGPT/User_Projects/RISCV_EE451/RISCV} 

        # Close_project  Not required. Will advise that Vivado sessions should be closed. 

        start_gui

        create_project  RISCV  ./VHDL/AMDprj -part xc7z020clg400-1 -force

        set_property target_language VHDL [current_project]

        add_files -norecurse  ./VHDL/model/RISCV.vhd
add_files -norecurse  C:/HDLGen-ChatGPT/User_Projects/RISCV_EE451/Package/MainPackage.vhd
add_files -norecurse  C:/HDLGen-ChatGPT/User_Projects/RISCV_EE451/RISCV_IF/VHDL/model/RISCV_IF.vhd 
add_files -norecurse  C:/HDLGen-ChatGPT/User_Projects/RISCV_EE451/RISCV_ID/VHDL/model/RISCV_ID.vhd 
add_files -norecurse  C:/HDLGen-ChatGPT/User_Projects/RISCV_EE451/RISCV_EX/VHDL/model/RISCV_EX.vhd 
add_files -norecurse  C:/HDLGen-ChatGPT/User_Projects/RISCV_EE451/RISCV_MEM/VHDL/model/RISCV_MEM.vhd 
add_files -norecurse  C:/HDLGen-ChatGPT/User_Projects/RISCV_EE451/RISCV_WB/VHDL/model/RISCV_WB.vhd 
add_files -norecurse  C:/HDLGen-ChatGPT/User_Projects/RISCV_EE451/RISCV_PCCU/VHDL/model/RISCV_PCCU.vhd 
add_files -norecurse  C:/HDLGen-ChatGPT/User_Projects/RISCV_EE451/RISCV_IM/VHDL/model/RISCV_IM.vhd 
add_files -norecurse  C:/HDLGen-ChatGPT/User_Projects/RISCV_EE451/RISCV_RB/VHDL/model/RISCV_RB.vhd 
add_files -norecurse  C:/HDLGen-ChatGPT/User_Projects/RISCV_EE451/RISCV_DEC/VHDL/model/RISCV_DEC.vhd 
add_files -norecurse  C:/HDLGen-ChatGPT/User_Projects/RISCV_EE451/RISCV_ALU/VHDL/model/RISCV_ALU.vhd 


        update_compile_order -fileset sources_1

        set_property SOURCE_SET sources_1 [get_filesets sim_1]

        add_files -fileset sim_1 -norecurse ./VHDL/testbench/RISCV_TB.vhd

        update_compile_order -fileset sim_1

        # Remove IOBs from snthesised schematics

        current_run [get_runs synth_1]

        set_property -name {STEPS.SYNTH_DESIGN.ARGS.MORE OPTIONS} -value -no_iobuf -objects [get_runs synth_1]


        # Save created wcfg in project

        set_property SOURCE_SET sources_1 [get_filesets sim_1]

        add_files -fileset sim_1 -norecurse ./VHDL/AMDPrj/RISCV_TB_behav.wcfg

        # save_wave_config {./VHDL/AMDprj/RISCV_TB_behav.wcfg}

    