<?xml version="1.0" ?>
<HDLGen>
	<genFolder>
		<vhdl_folder>RISCV/VHDL/model</vhdl_folder>
		<vhdl_folder>RISCV/VHDL/testbench</vhdl_folder>
		<vhdl_folder>RISCV/VHDL/ChatGPT</vhdl_folder>
		<vhdl_folder>RISCV/VHDL/ChatGPT/Backups</vhdl_folder>
		<vhdl_folder>RISCV/VHDL/AMDprj</vhdl_folder>
	</genFolder>
	<projectManager>
		<settings>
			<name>RISCV</name>
			<environment>C:/HDLGen-ChatGPT/User_Projects/RISCV_EE451</environment>
			<location>C:/HDLGen-ChatGPT/User_Projects/RISCV_EE451</location>
			<info>None</info>
		</settings>
		<EDA>
			<tool>
				<name>Xilinx Vivado</name>
				<dir>C:/Xilinx/Vivado/2019.1/bin/vivado.bat</dir>
				<version>2019.1</version>
			</tool>
		</EDA>
		<HDL>
			<language>
				<name>VHDL</name>
			</language>
		</HDL>
	</projectManager>
	<hdlDesign>
		<header>
			<compName>RISCV</compName>
			<title>RV32I RISC-V top level</title>
			<description>RV32I RISC-V top level</description>
			<authors>Mustafa Sultan</authors>
			<company>University of Galway</company>
			<email>m.sultan2@universityofgalway.ie</email>
			<date>14/11/2023</date>
		</header>
		<clkAndRst>
			<clkAndRst>
				<activeClkEdge>L-H</activeClkEdge>
				<rst>Yes</rst>
				<RstType>asynch</RstType>
				<ActiveRstLvl>1</ActiveRstLvl>
			</clkAndRst>
		</clkAndRst>
		<entityIOPorts>
			<signal>
				<name>clk</name>
				<mode>in</mode>
				<type>single bit</type>
				<description>clk signal</description>
			</signal>
			<signal>
				<name>rst</name>
				<mode>in</mode>
				<type>single bit</type>
				<description>rst signal</description>
			</signal>
			<signal>
				<name>ce</name>
				<mode>in</mode>
				<type>single bit</type>
				<description>Global chip enable</description>
			</signal>
			<signal>
				<name>ceOut</name>
				<mode>out</mode>
				<type>single bit</type>
				<description>temporary signal = ce</description>
			</signal>
		</entityIOPorts>
		<internalSignals>
			<signal>
				<name>PC</name>
				<type>bus(31 downto 0)</type>
				<description>Program counter (PC)</description>
			</signal>
			<signal>
				<name>ALUOut</name>
				<type>bus(31 downto 0)</type>
				<description>ALU output data</description>
			</signal>
			<signal>
				<name>rs1D</name>
				<type>bus(31 downto 0)</type>
				<description>Register Bank source data 1</description>
			</signal>
			<signal>
				<name>rs2D</name>
				<type>bus(31 downto 0)</type>
				<description>Register Bank source data 2</description>
			</signal>
			<signal>
				<name>PCPlus4</name>
				<type>bus(31 downto 0)</type>
				<description>PC + 4, stored in retrn address register on branch</description>
			</signal>
			<signal>
				<name>selMToWB</name>
				<type>bus(2 downto 0)</type>
				<description>select memory data to MToWB datapath</description>
			</signal>
			<signal>
				<name>selDToM</name>
				<type>bus(1 downto 0)</type>
				<description>sel memory write datapth</description>
			</signal>
			<signal>
				<name>MRd</name>
				<type>single bit</type>
				<description>Asserion combinationally reads DFrM = memory(ALUOut)</description>
			</signal>
			<signal>
				<name>MWr</name>
				<type>single bit</type>
				<description>Asserion synchronously write memory(ALUOut) = DToM, &amp;#10;when ce is asserted</description>
			</signal>
			<signal>
				<name>selA</name>
				<type>single bit</type>
				<description>Control ALU A input datapath</description>
			</signal>
			<signal>
				<name>selB</name>
				<type>single bit</type>
				<description>Control ALU B input datapath</description>
			</signal>
			<signal>
				<name>selBrBaseAdd</name>
				<type>single bit</type>
				<description>Selects branch base address datapath</description>
			</signal>
			<signal>
				<name>selNxtPC</name>
				<type>single bit</type>
				<description>SElects PCCU increment by 4 or branch PC address generation</description>
			</signal>
			<signal>
				<name>selALUOp</name>
				<type>bus(3 downto 0)</type>
				<description>ALU function select</description>
			</signal>
			<signal>
				<name>brAdd</name>
				<type>bus(31 downto 0)</type>
				<description>branch address</description>
			</signal>
			<signal>
				<name>instruction</name>
				<type>bus(31 downto 0)</type>
				<description>processor instruction</description>
			</signal>
			<signal>
				<name>extImm</name>
				<type>bus(31 downto 0)</type>
				<description>extended immediate signal</description>
			</signal>
			<signal>
				<name>WBDat</name>
				<type>bus(31 downto 0)</type>
				<description>Writeback data</description>
			</signal>
			<signal>
				<name>DToM</name>
				<type>bus(31 downto 0)</type>
				<description>data to memory</description>
			</signal>
			<signal>
				<name>MToWB</name>
				<type>bus(31 downto 0)</type>
				<description>memory to writeback</description>
			</signal>
			<signal>
				<name>selWBDat</name>
				<type>bus(1 downto 0)</type>
				<description>selevt WBDat datapath</description>
			</signal>
			<signal>
				<name>DFrM</name>
				<type>bus(31 downto 0)</type>
				<description>Main memory (MEM) component data output</description>
			</signal>
			<signal>
				<name>branch</name>
				<type>single bit</type>
				<description>Asserted when branch inbstruction ALU equality check is true</description>
			</signal>
		</internalSignals>
		<architecture>
			<archName>RTL</archName>
			<concurrentStmt>
				<label>ceOut_c</label>
				<statement>ceOut,ce</statement>
				<note>None</note>
			</concurrentStmt>
			<instance>
				<label>IF_i</label>
				<model>RISCV_IF</model>
				<port>clk,clk</port>
				<port>rst,rst</port>
				<port>ce,ce</port>
				<port>instruction,instruction</port>
				<port>PC,PC</port>
				<port>PCPlus4,PCPlus4</port>
				<port>selNxtPC,selNxtPC</port>
				<port>brAdd,brAdd</port>
			</instance>
			<instance>
				<label>ID_i</label>
				<model>RISCV_ID</model>
				<port>instruction,instruction</port>
				<port>WBDat,WBDat</port>
				<port>rs1D,rs1D</port>
				<port>rs2D,rs2D</port>
				<port>selALUOp,selALUOp</port>
				<port>selDToM,selDToM</port>
				<port>selNxtPC,selNxtPC</port>
				<port>selMToWB,selMToWB</port>
				<port>selWBDat,selWBDat</port>
				<port>MWr,MWr</port>
				<port>MRd,MRd</port>
				<port>selA,selA</port>
				<port>selB,selB</port>
				<port>selBrBaseAdd,selBrBaseAdd</port>
				<port>ce,ce</port>
				<port>clk,clk</port>
				<port>rst,rst</port>
				<port>extImm,extImm</port>
				<port>branch,branch</port>
			</instance>
			<instance>
				<label>EX_i</label>
				<model>RISCV_EX</model>
				<port>rs1D,rs1D</port>
				<port>rs2D,rs2D</port>
				<port>extImm,extImm</port>
				<port>PC,PC</port>
				<port>brAdd,brAdd</port>
				<port>ALUOut,ALUOut</port>
				<port>DToM,DToM</port>
				<port>branch,branch</port>
				<port>selALUOp,selALUOp</port>
				<port>selB,selB</port>
				<port>selA,selA</port>
				<port>selBrBaseAdd,selBrBaseAdd</port>
				<port>selDToM,selDToM</port>
			</instance>
			<instance>
				<label>MEM_i</label>
				<model>RISCV_MEM</model>
				<port>clk,clk</port>
				<port>MWr,MWr</port>
				<port>MRd,MRd</port>
				<port>DToM,DToM</port>
				<port>DFrM,DFrM</port>
				<port>address,ALUOut</port>
				<port>ce,ce</port>
			</instance>
			<instance>
				<label>WB_i</label>
				<model>RISCV_WB</model>
				<port>DFrM,DFrM</port>
				<port>selMToWB,selMToWB</port>
				<port>selWBDat,selWBDat</port>
				<port>WBDat,WBDat</port>
				<port>PCPlus4,PCPlus4</port>
				<port>ALUOut,ALUOut</port>
			</instance>
		</architecture>
		<testbench>
			<TBNote>No test plan created</TBNote>
		</testbench>
		<chatgpt>
			<commands>
				<VHDLModel># This ChatGPT message header is used with the HDLGen-generated HDL model template (which can include pseudo code logic descriptions) to generate the complete HDL model.&amp;#10;&amp;#10;Complete the following VHDL model&amp;#44; and output in a single formatted code box.&amp;#10;For each line between -- Title section start and -- Title section end&amp;#44; improve formatting of the text.&amp;#10;&amp;#10;For each line containing the prefix &amp;apos;--- &amp;apos;&amp;#44; generate VHDL code describing the logic&amp;#44; applying the following 10 rules&amp;#10;If a line includes an  arithmetic + or - operation&amp;#44; use &amp;quot;ieee.numeric_std&amp;quot; package type-casting function (on signals only) to ensure that the VHDL arithmetic assignment syntax is correct.&amp;#10;If a line includes a std_logic_vector signal as an index&amp;#44; convert the signal to an integer index using &amp;quot;ieee.numeric_std&amp;quot; package to_integer(unsigned())&amp;#10;If a line includes a shift operation&amp;#44; only use ieee.numeric_std package shift_left or shift_right functions&amp;#44; and do not use sra&amp;#44; srl&amp;#44; sll&amp;#44; shift_right_arithmetic functions.&amp;#10;If a line includes a &amp;gt;&amp;gt;&amp;gt; shift right arithmetic operation&amp;#44; use ieee.numeric_std package shift_right function&amp;#44; assuming signed data.&amp;#10;Leave all labels unchanged.&amp;#10;For each line containing &amp;apos;_c:&amp;apos; in the label&amp;#44; do not generate a sequential statement&amp;#44; do not remove the label containing &amp;apos;_c:&amp;apos;&amp;#44; remove the &amp;apos;---&amp;apos;&amp;#44; and remove &amp;apos;-- Default assignment&amp;apos;&amp;#10;Remove all assignments in if or case statements which duplicate the default assignment.&amp;#10;In assignments which include sign extending by one bit&amp;#44; use single quotes to define the single bit.&amp;#10;If using std_logic_vector signal as array index&amp;#44; use numeric_std to_integer function on unsigned signals.&amp;#10;Remove the &amp;apos;--- &amp;apos;  prefix from the line.&amp;#10;&amp;#10;Use if statements rather than conditional statements inside VHDL case or if statements&amp;#44;&amp;#10;Do not add any new library &amp;apos;use&amp;apos; statements&amp;#44; to avoid conflicts with the ieee.numeric_std package functions&amp;#44;&amp;#10;For all case statements&amp;#44; include &amp;quot;when others =&amp;gt; null;&amp;quot; before every occurrence of an &amp;quot;end case&amp;quot; statement.&amp;#10;&amp;#10;In VHDL processes&amp;#44; do not remove the line containing suffix &amp;apos;-- Default assignment&amp;apos;&amp;#44; and place it immediately before the VHDL statements generated for lines containing prefix &amp;apos;--- &amp;apos;.&amp;#10;&amp;#10;&amp;#10;=== For reference. Ignore the following lines.&amp;#10;=== List of Optional ChatGPT messages&amp;#44; which can be submitted individually after the above ChatGPT message&amp;#44; if the Chat GPT output is not fully correct.&amp;#10;If a line includes an  arithmetic + or - operation&amp;#44; use &amp;quot;ieee.numeric_std&amp;quot; package type-casting function (on signals only) to ensure that the VHDL arithmetic assignment syntax is correct.&amp;#10;Output &amp;lt;process NSAndOPDecode_p&amp;gt;&amp;#44; ensuring that arithmetic (+&amp;#44; -) operations use &amp;quot;ieee.numeric_std&amp;quot; package type-casting function (on signals only) to ensure that the VHDL arithmetic assignment syntax is supported by &amp;quot;ieee.numeric_std&amp;quot; arithmetitc operators&amp;#44;arithmetic operations use &amp;apos;ieee.numeric_std&amp;apos; package signal type conversion functions to ensure correct VHDL syntax.&amp;#10;Output &amp;lt;signalName&amp;gt; assignment&amp;#44; ensuring that arithmetic (+&amp;#44; -) operations use &amp;quot;ieee.numeric_std&amp;quot; package type-casting function (on signals only) to ensure that the VHDL arithmetic assignment syntax is supported by &amp;quot;ieee.numeric_std&amp;quot; arithmetitc operators&amp;#44;arithmetic operations use &amp;apos;ieee.numeric_std&amp;apos; package signal type conversion functions to ensure correct VHDL syntax.</VHDLModel>
				<VerilogModel># This ChatGPT message header is used with the HDLGen-generated HDL model template to generate the complete HDL model.&amp;#10;&amp;#10;Complete the following Verilog model&amp;#44; and output in a single formatted code box.&amp;#10;&amp;#10;For each line between // Title section start and // Title section end&amp;#44; improve formatting of the text.&amp;#10;&amp;#10;For each line containing the prefix &amp;apos;///&amp;apos;&amp;#44; replace with generated Verilog code for the logic described&amp;#44; and remove the &amp;apos;///&amp;apos;.&amp;#10;For each line containing &amp;apos;_c:&amp;apos; in the label&amp;#44; do not generate a sequential statement&amp;#44; do not remove the label containing &amp;apos;_c:&amp;apos;&amp;#44; remove the &amp;apos;///&amp;apos;&amp;#44; and remove &amp;apos;// Default assignment&amp;apos;&amp;#10;&amp;#10;Remove all assignments in if or case statements which duplicate the default assignment.&amp;#10;Use for loop (format for (i=0; i&amp;lt;32; i=i+1) around complete array signal assignments.&amp;#10;&amp;#10;Leave all labels unchanged.&amp;#10;Place the Verilog process line containing &amp;apos;// Default assignment&amp;apos;&amp;#44; unchanged&amp;#44; and immediately before the VHDL logic generated for lines containing prefix &amp;apos;///&amp;apos;.&amp;#10;Do not output the lines containing prefix &amp;apos;///&amp;apos;.&amp;#10;Include &amp;lt;= operator in assignments.&amp;#10;Make assignment bit widths compatible.</VerilogModel>
				<VHDLTestbench># This ChatGPT message header is used with the testbench signal declarations and test plan to generate the complete HDL testbench.&amp;#10;&amp;#10;In the table&amp;#44;&amp;#10;Input signal names are included in row 1&amp;#44; output signal names are included in row 2&amp;#44; signal radix is included in row 3.&amp;#10;TestNo is included in column 1&amp;#44; delay is included in column 2&amp;#10;&amp;#10;Create a single VHDL process stim_p&amp;#44; in a formatted code box&amp;#44; starting at row 4&amp;#44; and including the following&amp;#10;TestNo.&amp;#10;Comment in the Note column.&amp;#10;Assignment of every input signal&amp;#44; irrespective of its value. Do not assign any output signals. Use hexadecimal if signal format is greater or equal to 32 bits.&amp;#10;wait for (delay  * period); statement&amp;#44; where delay can be integer or real&amp;#44; after all signal stimulus for the row have been assigned&amp;#44; using delay value in the delay column&amp;#44; with brackets () around (delay * period).&amp;#10;Test if each output signal value (though not input signal values) matches the value included in its corresponding row&amp;#44; outputting a fail message if it does not match.&amp;#10;&amp;#10;Do not include any other delay statements&amp;#10;&amp;#10;=== For reference. Ignore the following lines.&amp;#10;=== List of Optional ChatGPT messages&amp;#44; which can be submitted individually after the above ChatGPT message&amp;#44; if the Chat GPT output is not fully correct.&amp;#10;None currently recorded in application settings file config.ini</VHDLTestbench>
				<VerilogTestbench># This ChatGPT message header is used with the testbench signal declarations and test plan to generate the complete HDL testbench.&amp;#10;&amp;#10;In the table&amp;#44;&amp;#10;Input signal names are included in row 1&amp;#44; output signal names are included in row 2&amp;#44; signal radix is included in row 3.&amp;#10;TestNo is included in column 1&amp;#44; delay is included in column 3&amp;#10;&amp;#10;Create a single VERILOG initial statement&amp;#44; in a formatted code box&amp;#44; starting at row 4&amp;#44; and including the following:&amp;#10;TestNo.&amp;#10;Comment taken from the Note column.&amp;#10;Assignment of every input signal&amp;#44; irrespective of its value.&amp;#10;Do not assign any output signals.&amp;#10;&amp;quot;# (delay value)&amp;quot; statement&amp;#44; where delay can be integer or real&amp;#44; after all signal stimulus for the row have been assigned&amp;#44; using delay value in the delay column&amp;#44; with brackets () around (delay * period)&amp;#44; not including any character before the # character&amp;#44; and not including TestNo in the line.&amp;#10;Test if each output signal value (though not input signal values) matches the value included in its corresponding row&amp;#44; outputting a fail message if it does not match.</VerilogTestbench>
			</commands>
		</chatgpt>
	</hdlDesign>
</HDLGen>