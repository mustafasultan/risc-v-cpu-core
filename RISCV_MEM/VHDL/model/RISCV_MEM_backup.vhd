library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.MainPackage.all;

entity RISCV_MEM is
    Port(
        clk : in std_logic;
        MWr : in std_logic;
        MRd : in std_logic;
        DToM : in std_logic_vector(31 downto 0);
        DFrM : out std_logic_vector(31 downto 0);
        address : in std_logic_vector(31 downto 0);
        ce : in std_logic
    );
end entity RISCV_MEM;

architecture RTL of RISCV_MEM is
    -- Internal signal declarations
    signal CSArray : array128x32 := ( others => (others => '0') ); -- possibly 128x32
    signal NSArray : array128x32 := ( others => (others => '0') ); -- possibly 128x32
begin

    NSDecode_p: process(MWr, DToM, address, ce, CSArray)
    begin
        NSArray <= CSArray; -- Default assignment
        if ce = '1' then
            if MWr = '1' then
                NSArray(to_integer(unsigned(address(6 downto 2)))) <= DToM;
            end if;
        end if;
    end process;

      DFrM_c: process(address, MRd, CSArray)
      begin
        if MRd = '1' then
            DFrM <= CSArray(to_integer(unsigned(address(6 downto 2))));
        else
            DFrM <= (others => '0');
        end if;
    end process;

--    DFrM_c: process(address)
--    begin
--        DFrM <= CSArray(to_integer(unsigned(address(6 downto 2))));
--    end process;

    stateReg_p: process(clk)
    begin
        if rising_edge(clk) then
            CSArray <= NSArray;
        end if;
    end process;

end RTL;
