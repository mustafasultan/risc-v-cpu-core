-- Title Section
-- VHDL testbench RISCV_MEM_TB
-- Generated by HDLGen, Github https://github.com/fearghal1/HDLGen

-- Component Name : RISCV_MEM
-- Title          : 32-bit register-based RISC-V main memory
-- Description    : refer to component hdl model fro function description and signal dictionary
-- Author(s)      : Fearghal Morgan
-- Company        : University of Galway
-- Email          : fearghal.morgan@universityofgalway.ie
-- Date           : 12/07/2023

-- library declarations
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Testbench entity declaration. No inputs or outputs
entity RISCV_MEM_TB is end entity RISCV_MEM_TB;

architecture behave of RISCV_MEM_TB is

-- unit under test (UUT) component declaration. Identical to component entity, with 'entity' replaced with 'component'
component RISCV_MEM is 
Port(
	clk : in std_logic;
	MWr : in std_logic;
	MRd : in std_logic;
	DToM : in std_logic_vector(31 downto 0);
	DFrM : out std_logic_vector(31 downto 0);
	address : in std_logic_vector(31 downto 0);
	ce : in std_logic 
	);
end component;

-- testbench signal declarations
signal testNo: integer; -- aids locating test in simulation waveform
signal endOfSim : boolean := false; -- assert at end of simulation to highlight simuation done. Stops clk signal generation.

-- Typically use the same signal names as in the VHDL entity, with keyword signal added, and without in/out mode keyword

signal clk: std_logic := '1';

signal MWr : std_logic;
signal MRd : std_logic;
signal DToM : std_logic_vector(31 downto 0);
signal DFrM : std_logic_vector(31 downto 0);
signal address : std_logic_vector(31 downto 0);
signal ce : std_logic;

constant period: time := 20 ns; -- Default simulation time. Use as simulation delay constant, or clk period if sequential model ((50MHz clk here)
 
begin

-- Generate clk signal, when endOfSim = FALSE / 0
clkStim: clk <= not clk after period/2 when endOfSim = false else '0';

-- instantiate unit under test (UUT)
UUT: RISCV_MEM-- map component internal sigs => testbench signals
port map
	(
	clk => clk, 
	MWr => MWr, 
	MRd => MRd, 
	DToM => DToM, 
	DFrM => DFrM, 
	address => address, 
	ce => ce
	);

-- Signal stimulus process
stim_p: process -- process sensitivity list is empty, so process automatically executes at start of simulation. Suspend process at the wait; statement
begin
	report "%N Simulation start, time = "& time'image(now);

	-- Apply default INPUT signal values. Do not assign output signals (generated by the UUT) in this stim_p process
	-- Each stimulus signal change occurs 0.2*period after the active low-to-high clk edge
	-- if signal type is
	-- std_logic, use '0'
	-- std_logic_vector use (others => '0')
	-- integer use 0
	MWr <= '0';
	MRd <= '0';
	DToM <= (others => '0');
	address <= (others => '0');
	ce <= '0';

	-- START Add testbench stimulus here
	-- === If copying a stim_p process generated by ChatGPT, delete the following lines from the beginning of the pasted code
	-- === Delete the -- === notes
	-- === stim_p: process
	-- === begin


	-- ==== If copying a stim_p process generated by ChatGPT, delete the following lines from the pasted code
	-- === wait;
	-- === end process stim_p;
	-- === end process stim_p;

	-- END Add testbench stimulus here
	-- Print picosecond (ps) = 1000*ns (nanosecond) time to simulation transcript
	-- Use to find time when simulation ends (endOfSim is TRUE)
	-- Re-run the simulation for this time
	-- Select timing diagram and use View>Zoom Fit
	report "%N Simulation end, time = "& time'image(now);
	endOfSim <= TRUE; -- assert flag to stop clk signal generation

	wait; -- wait forever
end process; 
end behave;