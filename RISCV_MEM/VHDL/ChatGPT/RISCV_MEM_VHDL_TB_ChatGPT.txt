# This ChatGPT message header is used with the testbench signal declarations and test plan to generate the complete HDL testbench.

In the table,
Input signal names are included in row 1, output signal names are included in row 2, signal radix is included in row 3.
TestNo is included in column 1, delay is included in column 2

Create a single VHDL process stim_p, in a formatted code box, starting at row 4, and including the following
TestNo.
Comment in the Note column.
Assignment of every input signal, irrespective of its value. Do not assign any output signals. Use hexadecimal if signal format is greater or equal to 32 bits.
wait for (delay  * period); statement, where delay can be integer or real, after all signal stimulus for the row have been assigned, using delay value in the delay column, with brackets () around (delay * period).
Test if each output signal value (though not input signal values) matches the value included in its corresponding row, outputting a fail message if it does not match.

=== For reference. Ignore the following lines.
=== List of Optional ChatGPT messages, which can be submitted individually after the above ChatGPT message, if the Chat GPT output is not fully correct.
None currently recorded in application settings file config.ini

signal testNo: integer;
signal period: time := 20 ns;
signal MWr : std_logic;
signal MRd : std_logic;
signal DToM : std_logic_vector(31 downto 0);
signal DFrM : std_logic_vector(31 downto 0);
signal address : std_logic_vector(31 downto 0);
signal ce : std_logic;

Inputs          ce  MWr address  DToM     MRd              
Outputs                                       DFrM         
TestNo  delay   1'b 1'b 32'h     32'h     1'b 32'h         Note
1       1       0   1   00000004 deadbeef 0   00000000     synch write to add = 4 with ce deasserted
2       1       1   1   00000004 deadbeef 0   00000000     synch write to add = 4 with ce asserted
3       1       1   1   00000008 c001cafe 0   00000000     synch write to add = 8 with ce asserted
4       0.001   1   0   00000004 ffffffff 1   deadbeef     combinational read of add = 4
        0.999   1   0   00000004 ffffffff 1   deadbeef     combinational read of add = 4
5       0.001   1   1   00000008 a5a55a5a 1   c001cafe     combinational read of add 4, synch write of add = 4
6       0.999   1   1   00000008 a5a55a5a 1   a5a55a5a     combinational read of add 4, synch write of add = 4