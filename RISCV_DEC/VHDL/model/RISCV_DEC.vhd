-- Title Section Start
-- Generated by HDLGen, Github https://github.com/fearghal1/HDLGen-ChatGPT, on 05-November-2023 at 10:40

-- Component Name : RISCV_DEC
-- Title          : RISC-V Decoder

-- Author(s)      : Mustafa Sultan
-- Organisation   : University of Galway
-- Email          : m.sultan2@universityofgalway.ie
-- Date           : 05/11/2023

-- Description
-- Decodes instruction(31:0), generating
-- RWr, rd(4:0), rs1(4:0), rs2(4:0), extImm(31:0), selA, selB, selALUOp(3:0), 
-- selDToM(1:0), selBrBaseAdd, selMToWB(2:0), MWr, MRd, selWBDat, selNxtPC

-- entity signal dictionary
-- instruction    Processor instruction(31:0)
-- RWr            Asserted to synchronously write RB(rd) = WBDat
-- rd             Register bank synchronous write destination register
-- rs1            Register bank source register 1 address
-- rs2            Register bank source register 2 address
-- extImm         Extended immediate
-- selA           Select ALU input data A
-- selB           Select ALU input data B
-- selALUOp       Select ALU operation
-- selDToM        Select memory input datapath
-- selBrBaseAdd   Select branch base address
-- selMToWB       Select memory read data to writeback datapath
-- MWr            Assert to synchronously write memory(ALUOut) = DToM
-- MRd            Assert to combinationally read MToWB = memory(ALUOut)
-- selWBDat       Select WBDat datapath
-- selNxtPC       Assert to load brAdd in PC
--                Deassert to increment PC byte address by 4
-- branch         Asserted (h) on a branch instruction

-- internal signal dictionary
-- None

-- Title Section End

-- library declarations
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.MainPackage.all;

-- entity declaration
entity RISCV_DEC is 
    Port(
        instruction : in std_logic_vector(31 downto 0);
        RWr : out std_logic;
        rd : out std_logic_vector(4 downto 0);
        rs1 : out std_logic_vector(4 downto 0);
        rs2 : out std_logic_vector(4 downto 0);
        extImm : out std_logic_vector(31 downto 0);
        selA : out std_logic;
        selB : out std_logic;
        selALUOp : out std_logic_vector(3 downto 0);
        selDToM : out std_logic_vector(1 downto 0);
        selBrBaseAdd : out std_logic;
        selMToWB : out std_logic_vector(2 downto 0);
        MWr : out std_logic;
        MRd : out std_logic;
        selWBDat : out std_logic_vector(1 downto 0);
        selNxtPC : out std_logic;
        branch : in std_logic
    );
end entity RISCV_DEC;

architecture Combinational of RISCV_DEC is
    -- Internal signal declarations
    -- None
begin

    selALUOp_p: process(instruction, branch)
    begin
        selALUOp <= (others => '0'); -- Default assignment
        
        case instruction(6 downto 0) is
            when "1100011" =>
                case instruction(14 downto 12) is
                    when "000" => selALUOp <= "1010"; -- BEQ 
                    when "001" => selALUOp <= "1011"; -- BNE
                    when "100" => selALUOp <= "1100"; -- BLT
                    when "101" => selALUOp <= "1101"; -- BGE
                    when "110" => selALUOp <= "1110"; -- BLTU
                    when "111" => selALUOp <= "1111"; -- BGEU
                    when others => null;
                end case;
            when "0010011" =>
                case instruction(14 downto 12) is
                    when "000" => null; -- ADDI
                    when "010" => selALUOp <= "1000"; -- SLTI
                    when "011" => selALUOp <= "1001"; -- SLTIU
                    when "100" => selALUOp <= "0100"; -- XORI
                    when "110" => selALUOp <= "0011"; -- ORI
                    when "111" =>
                        case instruction(30) is
                            when '0' => selALUOp <= "0110"; -- SRLI
                            when '1' => selALUOp <= "0111"; -- SRAI
                            when others => null;
                        end case;
                    when others => null;
                end case;
            when "0110011" =>
                case instruction(14 downto 12) is
                    when "000" =>
                        case instruction(30) is
                            when '0' => null;               -- ADD
                            when '1' => selALUOp <= "0001"; -- SUB
                            when others => null;
                        end case;
                    when "001" => selALUOp <= "0101"; -- SLL
                    when "010" => selALUOp <= "1000"; -- SLT
                    when "011" => selALUOp <= "1001"; -- SLTU
                    when "100" => selALUOp <= "0100"; -- XOR
                    when "101" =>
                        case instruction(30) is
                            when '0' => selALUOp <= "0110"; -- SRL
                            when '1' => selALUOp <= "0111"; -- SRA
                            when others => null;
                        end case;
                    when "110" => selALUOp <= "0011"; -- OR
                    when "111" => selALUOp <= "0010"; -- AND
                    when others => null;
                end case;
            when others => null;
        end case;
    end process;

    control_p: process(instruction, branch)
    begin
        RWr <= '0'; -- Default assignment 
        selA <= '0'; -- Default assignment 
        selB <= '0'; -- Default assignment 
        selDToM <= (others => '0'); -- Default assignment 
        selBrBaseAdd <= '0'; -- Default assignment 
        selMToWB <= (others => '0'); -- Default assignment 
        MWr <= '0'; -- Default assignment 
        MRd <= '0'; -- Default assignment 
        selWBDat <= (others => '0'); -- Default assignment 
        selNxtPC <= '0'; -- Default assignment 

        case instruction(6 downto 0) is
            when "0110111" => -- Load Upper Immediate Instruction (Registered)
                RWr <= '1';
            when "0010111" => -- AUIPC Instruction
                RWr <= '1';
                selA <= '1';                
            when "0000011" => -- Memory Load Instructions
                RWr <= '1';
                MRd <= '1';
                selWBDat <= "01";
                case instruction(14 downto 12) is
                    when "000" => selMToWB <= "010"; -- LB 
                    when "001" => selMToWB <= "001"; -- LH
                    when "010" => selMToWB <= "000"; -- LW (Default)
                    when "100" => selMToWB <= "100"; -- LBU
                    when "101" => selMToWB <= "011"; -- LHU 
                    when others => null;
                end case;
            when "0100011" => -- Memory Store Instructions 
                MWr <= '1';
                case instruction(14 downto 12) is
                    when "000" => selDToM <= "10"; -- SB
                    when "001" => selDToM <= "01"; -- SH
                    when "010" => selDToM <= "00"; -- SW (Default)
                    when others => null;
                end case;
            when "0110011" => -- Register-register instructions
                RWr <= '1';
                selB <= '1';
            when "0010011" => -- Register-immediate instructions
                RWr <= '1';
            when "1100011" => -- Branch instructions
                selB <= '1';
                if branch = '1' then 
                    selNxtPC <= '1';
                end if;
            when "1101111" => -- Jump And Link Instruction
                RWr <= '1';
                selWBDat <= "10";
                selNxtPC <= '1';
            when "1100111" => -- Jump And Link Instruction (Registered)
                RWr <= '1';
                selNxtPC <= '1';
                selBrBaseAdd <= '1';
                selWBDat <= "10";
            when others => null;
        end case;
    end process;

    data_p: process(instruction)
    begin
        rd <= (others => '0'); -- Default assignment 
        rs1 <= (others => '0'); -- Default assignment 
        rs2 <= (others => '0'); -- Default assignment 
        extImm <= (others => '0'); -- Default assignment 

        case instruction(6 downto 0) is
            when "0110111" => -- LUI instruction
                extImm(31 downto 12) <= instruction(31 downto 12);
                rd <= instruction(11 downto 7);
            when "0010111" => -- AUIPC instruction
                extImm(31 downto 12) <= instruction(31 downto 12);
                rd <= instruction(11 downto 7);
            when "1101111" => -- JAL instruction
                extImm(31 downto 20) <= (others => instruction(31));
                extImm(19 downto 12) <= instruction(19 downto 12);
                extImm(11) <= instruction(20);
                extImm(10 downto 1) <= instruction(30 downto 21);
                rd <= instruction(11 downto 7);
            when "1100011" => -- Branch instructions
                extImm(12) <= instruction(31);
                extImm(11) <= instruction(7);
                extImm(10 downto 5) <= instruction(30 downto 25);
                extImm(4 downto 1) <= instruction(11 downto 8);
                rs1 <= instruction(19 downto 15);
                rs2 <= instruction(24 downto 20);
            when "1100111" => -- JALR instruction
                extImm(31 downto 25) <= (others => instruction(31));
                extImm(11 downto 0) <= instruction(31 downto 20);
                rd <= instruction(11 downto 7);
                rs1 <= instruction(19 downto 15);
            when "0000011" => -- Memory load instructions
                extImm(11 downto 0) <= instruction(31 downto 20);
                rd <= instruction(11 downto 7);
                rs1 <= instruction(19 downto 15);
            when "0010011" => -- Register extImmediate instructions
                extImm(31 downto 25) <= (others => instruction(31));
                extImm(11 downto 0) <= instruction(31 downto 20);
                rd <= instruction(11 downto 7);
                rs1 <= instruction(19 downto 15);
            when "0100011" => -- Memory store instructions
                extImm(11 downto 5) <= instruction(31 downto 25);
                extImm(4 downto 0) <= instruction(11 downto 7);
                rs1 <= instruction(19 downto 15);
                rs2 <= instruction(24 downto 20);
            when "0110011" => -- Register register instructions
                rd <= instruction(11 downto 7);
                rs1 <= instruction(19 downto 15);
                rs2 <= instruction(24 downto 20);
            when others => null;
        end case;
    end process;

end Combinational;
