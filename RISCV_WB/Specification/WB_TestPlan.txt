Input signals               selWBDat ALUOut   DFrM     selMToWB PCPlus4
Input signal radix          2'b      32'h     32'h     3'b      32'h      
Outputs                                                                   WBDat
Output signal radix                                                       32'h     
TestNo delay                                                                       Note
1      1                    00       5a5a5a5a deadbeef 000      c001cafe  5a5a5a5a ALUOut


Complete test plan, generate HDL and simulate HDL model
Use hex values such as deadbeef, c001cafe, f00dcafe, c001100c, a5a5a5a5, 3c3c3c3c, 5a5a5a5a, ffffffff

-- === Test Specification / Test Table Notes ===
Title: RV32I RISC-V processor Writeback component
	   Test table (input signals and expected outputs)
Created by: Fearghal Morgan
Date: Oct 2023
Component type (include * to highlight)
Combinational(*)
Sequential () 

Notes on the use of the Test Table with HDLGen-ChatGPT (https://github.com/fearghal1/HDLGen-ChatGPT)
1. Copy/paste the test table (excluding the header text) into the HDLGen-ChatGPT 'Test Plan' UI box.
2. Test table structure
     Use only single character spacing in this file. Do not use TAB spacing in this file.
     e.g, if using Notepad++ (https://notepad-plus-plus.org/downloads/), toggle 'make all characters visible' option to view space/TAB characters.
     Row 1 includes input signal name headings
     Row 2 includes output signal name headings
     Row 3 includes signal radix
     Row 4 includes the following column headings (Column 1: TestNo, Column 2: delay, Other: Note)
     Rows 5 onwards includes test data
3. Signal radix format: 1'b is 1-bit binary (e.g, 0, 1), 16'b is 16-bit binary (e.g, 1011111011101111), 16'h is 16-bit hexadecimal (e.g, beef).
4. Notes column text is optional, providing useful information on the individual test.
5. Sequential logic components
     clk signal: testbench HDL includes a clock (signal clk) strobe stimulus process, with default 20ns (50MHz) 'period'.
     rst (reset) signal: initially asserted, deasserting 0.2 * period after the first active clk edge.
6. Table delay values
     Combinational logic components: number of period (20ns) delays to be included, following the application of the test input signals 
     Sequential logic components:
       Number of clock periods to be applied, following  the application of the test input signals. Value can be integer or real.
       Examples:
         1:   delay 1 clk period,   ending 0.2 x period after the active clk edge
         3:   delay 3 clk period,   ending 0.2 x period after the active clk edge
         0.2: delay 0.2 clk period, ending 0.4 x period after the active clk edge
              This enables simulation ending 0.6*period prior to the next active clk edge, enabling checking of unregistered output signals
         0.8: delay 0.8 clk period, ending at the default point, i.e, 0.2 x period after the active clk edge